CTA Arduino Shield Libraries for KiCAD
===================================

This repository contains the .pretty folders, footprints used for printed circuit boards layout editor PcbNew, part of KiCAD EDA Suite.

For instructions on how to work with KiCAD libraries, see the link https://code.google.com/p/opendous/wiki/KiCADTutorialAddingLibraries


About the Authors
-----------------

This project started by [Centro de Tecnologia Acadêmica](http://cta.if.ufrgs.br) of Instituto de Física / Universidade Federal do Rio Grande do Sul (IF/UFRGS).

Gabriel Krieger Nardon - Inicial Dev


Licensing Information
---------------------

CTA Arduino Shield Libraries for KICAD
Copyright (C) 2014 by the Authors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
